<?php
namespace Dayone\Issuer;


class Loreal {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\LorealServiceProvider');
        return 'Loreal::index';
    }


}